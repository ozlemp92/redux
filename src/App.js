import React from "react";
import { connect } from 'react-redux';
import Split from 'react-split';
import '../src/App.scss';
import SplitComponent from './components/Split';
import Nav from './components/Navbar';

const App = props => {
  console.log(props.list)
  return (
    <>
      <Nav />
      <SplitComponent/>
    </>
  );
}
const mapStateProps = state => {
  return {
    newList: state.newList
  }
}
export default connect(mapStateProps)(App);
