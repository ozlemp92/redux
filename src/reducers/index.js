
const INITIAL_STATE = {
    newList: [],
    gridData: [
        {
            id: 1,
            kontrat: "2018",
            teklif: "1000",
            data: "Satış"
        },
        {
            id: 2,
            kontrat: "2020",
            teklif: "5600",
            data: "Alış"
        },
    ],
    sizeBottomVertical: [],
    sizeTopVertical: [],
    sizeHorizontal: []
}
export const reducer = (state = INITIAL_STATE, action) => {
    debugger;
    switch (action.type) {
        case 'ADD_PRODUCT':
            return { ...state, newList: [...state.newList, action.payload.fields] }
        case 'SIZE_CALCULATE_HORIZONTAL':
            return { ...state, sizeHorizontal: [state.sizeHorizontal, action.payload] }
        case 'SIZE_CALCULATE_TOPVERTICAL':
            return { ...state, sizeTopVertical: [state.sizeTopVertical, action.payload] }
        case 'SIZE_CALCULATE_BOTTOMVERTICAL':
            return { ...state, sizeBottomVertical: [state.sizeBottomVertical, action.payload] }
        default: return state;
    }


}