import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { default as diskette } from '../assests/diskette.svg';
import { default as user } from '../assests/user.svg';
export default class NavHome extends React.Component {
  render() {
    return (
      <>
        <div className="main-navbar">
          <div className="nav-label">EPİAŞ</div>
          <div className="flex-column" style={{padding: "10px"}}>
            <div className="flex-row">
              <div className="flex-column">
                <img  className="icon-diskette" src={diskette} />
              </div>
              <div className="flex-column">
                <label className="margin-label">Çalışma Alanı 1</label>
              </div>
              <div className="flex-column">
                <img className="icon-user" src={user} />
              </div>
              <div className="flex-column">
              <label style={{ color:"white" }}>Merhaba Ayhan</label>
              </div>
            </div>

          </div>
        </div>

      </>
    );
  }
}