
import React, { useState } from 'react';
import { add } from '../actions';
import { connect } from 'react-redux';
import { Table } from 'react-bootstrap';
import './../styles/style.scss';
const ItemInsert = props => {
    function handleClick() {
        setField("inherit");
    }
    const [record, setRecord] = useState({ fields: { firstField: "", secondField: "", thirdField: "", fourthField: "" } });
    const handleChange = e => {
        const { name, value } = e.target;
        setRecord((prevState) => ({
            ...prevState,
            fields: {
                ...prevState.fields,
                [name]: value
            }
        }));
    };

    const [fieldDisplay, setField] = useState('none');
    return (
        <>
            <Table striped bordered hover size="sm">
                <tbody>
                    {props.newList && props.newList.map(a =>
                    (
                        <tr>
                            <td className="">
                                <span>{a.firstField}</span>
                            </td>
                            <td className="">
                                <span>{a.secondField}</span>
                            </td>
                            <td className="">
                                <span>{a.thirdField}</span>
                            </td>
                            <td className="">
                                <span>{a.fourthField}</span>
                            </td>
                        </tr>
                    ))
                    }
                </tbody>
            </Table>
            <table>
                <tr style={{ display: fieldDisplay }}>
                    <td className="">
                        <br />
                        <td>
                            <input className="insert-field"
                            type="text" value={record.firstField}
                                type="text"
                                placeholder="No Giriniz"
                                onChange={handleChange}
                                name="firstField" ></input>
                        </td>
                        <td>
                            <input className="insert-field"
                            type="text" value={record.secondField}
                                type="text"
                                placeholder="Kontrat Giriniz"
                                onChange={handleChange}
                                name="secondField" ></input>
                        </td>
                        <td>
                            <input className="insert-field"
                            type="text" value={record.thirdField}
                                type="text"
                                placeholder="Teklif Giriniz"
                                onChange={handleChange}
                                name="thirdField" ></input>
                        </td>
                        <td>
                            <input className="insert-field"
                            type="text" value={record.fourthField}
                                type="text"
                                onChange={handleChange}
                                placeholder="Data Giriniz"
                                name="fourthField" ></input>
                        </td>
                        <td>
                            <button onClick={() => props.add(record)}>Kaydet</button>
                        </td>
                    </td>
                </tr>
                <tr>
                    <td className="">
                        <br />
                        <button onClick={handleClick}>Yeni Ekle</button>
                    </td>
                </tr>
            </table>
        </>
    )
}
const mapStateProps = state => {
    return {
        newList: state.newList,
        list: state.list
    }
}
export default connect(mapStateProps, { add })(ItemInsert);


