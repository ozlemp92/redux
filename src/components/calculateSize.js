
import React from 'react';
import { connect } from 'react-redux';
import './../styles/style.scss';
import { calculateHorizontal } from '../actions';
import { calculateTopVertical } from '../actions';
import { calculateBottomVertical } from '../actions';
const calculateSize = props => {
    debugger;
    return (
        <div className="flex-container">
            <div className="flex-float-right">Ayarlar</div>
            <div className="flex-float-right">Yatay Pencere Değerleri</div>
            <div className="flex-float-right">{props.sizeHorizontal.length != 0 &&(props.sizeHorizontal[1][0]+ "-"+ props.sizeHorizontal[1][1])} </div>
            <div className="flex-float-right">Üst Dikey Pencere Değerleri</div>
            <div className="flex-float-right">{props.sizeTopVertical.length != 0 &&(props.sizeTopVertical[1][0]+ "-"+ props.sizeTopVertical[1][1])}</div>
            <div className="flex-float-right">Alt Dikey Pencere Değerleri</div>
            <div className="flex-float-right">{props.sizeBottomVertical.length != 0 &&(props.sizeBottomVertical[1][0]+ "-"+ props.sizeBottomVertical[1][1])}</div>
        </div>
    )
}
const mapStateProps = state => {
    return {
        sizeBottomVertical: state.sizeBottomVertical,
        sizeHorizontal: state.sizeHorizontal,
        sizeTopVertical : state.sizeTopVertical
    }
}
export default connect(mapStateProps, { calculateHorizontal,calculateTopVertical,calculateBottomVertical })(calculateSize);